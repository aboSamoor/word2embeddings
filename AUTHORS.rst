Polyglot is maintained by Rami Al-Rfou' and various contributors:

Development Lead
````````````````

- Rami Al-Rfou' <ralrfou@cs.stonybrook.edu>
- Bryan Perozzi <bperozzi@cs.stonybrook.edu>


Patches and Suggestions
````````````````````````

- Vivek Kulkarni
- Abhinav Gupta
- Vishwas Sharma
