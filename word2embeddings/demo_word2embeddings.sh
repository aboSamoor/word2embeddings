VOC_FILE=$1
TRAIN_FILE=$2
DEV_FILE=$3
LANG=en
LOG_LEVEL=DEBUG
BASE_DIR=$(dirname $0)
python $BASE_DIR/create_embeddings.py \
        --vocabulary $VOC_FILE \
        --train-file $TRAIN_FILE \
        --dev-file $DEV_FILE \
        --language $LANG \
        --left-context 2 \
        --right-context 2 \
        --epochs-limit 30 \
        --examples-limit 5.0e9 \
        --batch-size 16 \
        --learning-method fan_in \
        --regularization-rate 0.0 \
        --hidden-layers 32 \
        --dump-period 3600 \
        --save-best false \
        --period examples \
        --validation-period 3e6 \
        --word-embedding-size 64 \
        --learning-rate 0.1 \
        -l $LOG_LEVEL
